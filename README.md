# ChickTechAIWorkshopNCSU

This is the tutorial for the ChickTech AI Workshop at NCSU on 5/18/19

### GitLab

1.  Sign into your [GitLab](https://gitlab.com) account or create one if you don't already have one.
2.  Fork the repo by going to [https://gitlab.com/staylornc/chicktechaiworkshopncsu](https://gitlab.com/staylornc/chicktechaiworkshopncsu)
3.  



### Install Anaconda

###



# Useful Links
[GitLab](https://gitlab.com) - Code repository. Continue to add projects here to create your portfolio  
[Anaconda](https://www.anaconda.com/distribution/) - Data Science Development Environment  
[Markdown Cheatsheet](https://www.markdownguide.org/basic-syntax/) - The language used for Readme files in your GitLab projects and Jupyter notebook text blocks

# Next Steps
1.  Perform the same exercise with a different dataset